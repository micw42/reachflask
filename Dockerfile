FROM ubuntu:latest

COPY . /app
WORKDIR /app

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev

RUN pip install -r requirements.txt

ENTRYPOINT [ "python3" ]
CMD [ "flask_vis.py" ]